#!/bin/perl
use	strict																;
use	warnings															;
use utf8																;
use	open		qw/ :std :utf8										/	;

use	POSIX																;
use	TFIDFSim															;

# Main variables
my	( %match,$tfidf_mf05,%tfidf_mf05								,
	$tfidf_pf,%tfidf_pf,$sim,%sim,$doc_mf05							)	;
# Locations of inputs and outputs
my	$in			= "../share/in/"										;
my	$out		= "../share/out/"										;
my	$nam0		= "TM6A16_MF2005-DICT.csv"								;
my	$nam1		= "parflow.manual.2-15-16-DICT.csv"						;
my	$nam_out	= "MF05_PF-Match.csv"									;


# Derive tf-idf from sought files
my	$path_in	= $in.$nam0												;
$tfidf_mf05		= tfidf	( $path_in									)	;
%tfidf_mf05		= %$tfidf_mf05											;

$path_in		= $in.$nam1												;
$tfidf_pf		= tfidf	( $path_in									)	;
%tfidf_pf		= %$tfidf_pf											;

# Compare each tf-idf by way of cosine similarity
$sim			= sim	( \%tfidf_mf05,\%tfidf_pf					)	;
%sim			= %$sim													;

# Print terms in order of similarity
HASH_MAX:
for	$doc_mf05	( keys	%sim										)	{
	$match{ $doc_mf05 }	= hash_max	( %{ $sim{ $doc_mf05		}	}	)	;
																		}
my	@matches	= sort												{
		$sim{ $b }{ $match{ $b } }	<=> $sim{ $a }{ $match{ $a	}	}
																	}
	keys	%match														;
# Output nearest match between corpora per term
my	$path_out	= $out.$nam_out											;
my	$fh			= undef													;
open			( $fh,	">",	$path_out							)
	||	die				"$0: can't open $path_out for reading: $!"		;
PRINT:
foreach	$doc_mf05	( @matches										)	{
	print	$fh		"$doc_mf05\t"
		."$match{ $doc_mf05 }\t"
		."$sim{ $doc_mf05 }{ $match{ $doc_mf05 } }\n"						;
																		}
close			( $fh												)
	||	die				"$path_out: $!"									;

# Cleanly run
print			"\n"													;


