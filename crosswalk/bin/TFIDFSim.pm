package	TFIDFSim												;
use		strict													;
use		warnings												;
use		utf8													;
use		open		qw/ :std :utf8							/	;
#use		threads													;
#use		threads::shared											;

use		POSIX													;
use		Exporter												;

our		@ISA		= qw( Exporter							)	;
our		@EXPORT_OK	= qw( tfidf sim hash_max				)	;
our		@EXPORT		= qw( tfidf	sim hash_max				)	;


sub		tfidf													{
# Ingests CSV-formatted dictionary whose keys and values
# are delimited by tabs and whose words by spaces
# Returns a hash (terms) of hashes (words) whose values
# are the TFIDF's
	my			( @field,@terms,$term,%count,%seen				,
		%termct,%idf,$doc,%tf,%tfidf							)	;
	my			$fh		= undef										;
	open		( $fh,	"<",	$_[0]							)
		||	die			"$0: can't open $_[0] for reading: $!"		;
# Process each document for this particular corpus
	COUNT:
	while		( my $row	= <$fh>								)	{
		@field				= split ( "\t",	$row					)	;
		@terms				= split	/\s+/,	lc $field[1]				;
		foreach	$term		( @terms								)	{
			$count{ $field[0] }{ $term }	++								;
			$seen{ $term }					++								;
			$termct{ $term }				++
				if	( $seen{ $term }	== 1							)	;
																		}
		%seen				=										()	;
																	}
	close		( $fh											)
		||	die	"$_[0]: $!"											;
# Calculate an inverse document frequency per term per corpus
	IDF:
	for			$term	( keys	%termct							)	{
		$idf{ $term }	= log10	( $termct{ $term			}	)
			/ scalar keys %count										;
																	}
# Calculate a term frequency and a tf-idf per term per doc per corpus
	TFIDF:
	for			$doc	( keys	%count							)	{
		for			$term	( keys	%{ $count{ $doc			}	}	)	{
			$tf{ $doc }{ $term }			= $count{ $doc }{ $term		}
				/ scalar keys %{ $count{ $doc						}	}	;
			$tfidf{ $doc }{ $term }			= $tf{ $doc }{ $term		}
				* $idf{ $term											}	;
																		}
																	}
	return		\%tfidf												;
																}

sub		sim													{
# From a hash of hashes as described above,
# returns a hash of hashes of each term of the first corpus
# by each term of the second corpus whose value
# represents the cosine similarity between the two
	my		( $doc0,$term0,$sumsq0,$doc1,$term1				,
		$sumsq1,$sumprod,%sim								)	;
	SIM:
	for		$doc0	( keys	%{$_[0]}						)	{
		SUMSQ0:
		for	$term0	( keys	%{ $_[0]{ $doc0				}	}	)	{
			$sumsq0		+= $_[0]{ $doc0 }{ $term0 }	** 2				;
																	}
		$sumsq0		= sqrt	( $sumsq0							)	;
		for	$doc1	( keys	%{$_[1]}							)	{
			SUMSQ1:
			for	$term1	( keys	%{ $_[1]{ $doc1				}	}	)	{
				$sumsq1		+= $_[1]{ $doc1 }{ $term1 }	** 2				;
																		}
			$sumsq1					= sqrt		( $sumsq1			)	;
			SUMPROD:
			for	$term0	( keys	%{ $_[0]{ $doc0				}	}	)	{
				$sumprod	+= $_[0]{ $doc0 }{ $term0					}	
					* $_[1]{ $doc1 }{ $term0							}
					if	( exists	$_[1]{ $doc1 }{ $term0			}	)	;
																		}
			$sim{ $doc0 }{ $doc1 }	= $sumprod
				/ $sumsq0	/ $sumsq1									;
																	}
																}
	return	\%sim												;
															}

sub		hash_max	( \% )										{
# This subroutine has been minorly modified from the original,
# which was posted as an answer
# on the Question-and-answer (Q$A) site, Stack Overflow.
# Its license and citation are provided in the following:
	my		$hash			= shift									;
	my		( $key,@keys )	= keys		%$hash						;
	my		( $big,@vals )	= values	%$hash						;
	MAX:
	for						( 0	.. $#keys						)	{
		if	( $vals[$_]	> $big										)	{
			$big	= $vals[$_]												;
			$key	= $keys[$_]												;
																		}
																	}
	return	$key													;
																}


1																;
