#!/bin/bash

find "../share/in/" | grep "output.dat" > "output.txt";
while IFS='' read -r line || [[ -n "$line" ]]; do
	pth=${line%output.dat}
	grep -a -A 40 -e "VOLUMETRIC.*12.*120" $line > "${pth}outputVOL.dat"
done < "output.txt"
