      SUBROUTINE GWF2BAS7OT(KSTP,KPER,ICNVG,ISA,IGRID,BUDPERC)
C     ******************************************************************
C     OUTPUT TIME, VOLUMETRIC BUDGET, HEAD, AND DRAWDOWN
C     ******************************************************************
C
C        SPECIFICATIONS:
C     ------------------------------------------------------------------
      USE GLOBAL,      ONLY:ITMUNI,IOUT,NCOL,NROW,NLAY,HNEW,STRT,DDREF
      USE GWFBASMODULE,ONLY:DELT,PERTIM,TOTIM,IHDDFL,IBUDFL,
     1                      MSUM,VBVL,VBNM,IDDREF
C     ------------------------------------------------------------------
C
      CALL SGWF2BAS7PNT(IGRID)
C
C1------CLEAR PRINTOUT FLAG (IPFLG)
      IPFLG=0
      BUDPERC=1.E30
C
C
      IF(ISA.EQ.0) THEN
         WRITE(IOUT,9) KSTP,KPER
    9    FORMAT(1X,/9X,'NO FLOW EQUATION TO SOLVE IN TIME STEP',I5,
     1      ' OF STRESS PERIOD',I3,/1X,'ALL HEADS ARE 0.0')
         IPFLG=1
      END IF
C
C2------IF ITERATIVE PROCEDURE FAILED TO CONVERGE PRINT MESSAGE
      IF(ICNVG.EQ.0) THEN
         WRITE(IOUT,17) KSTP,KPER
   17    FORMAT(1X,/9X,
     1 '****FAILED TO MEET SOLVER CONVERGENCE CRITERIA IN TIME STEP',
     2   I5,' OF STRESS PERIOD ',I4,'****')
         IPFLG=1
      END IF
C
C3------IF HEAD AND DRAWDOWN FLAG (IHDDFL) IS SET WRITE HEAD,
C3------DRAWDOWN, AND IBOUND IN ACCORDANCE WITH FLAGS IN IOFLG.
      IF(IHDDFL.EQ.0) GO TO 100
C
      CALL SGWF2BAS7H(KSTP,KPER,IPFLG,ISA)
      CALL SGWF2BAS7D(KSTP,KPER,IPFLG,ISA)
      CALL SGWF2BAS7IB(KSTP,KPER)
C
  100 CONTINUE
  
C4------PRINT TOTAL BUDGET IF REQUESTED
      IF(IBUDFL.EQ.0) GO TO 120
      CALL SGWF2BAS7V(MSUM,VBNM,VBVL,KSTP,KPER,IOUT,BUDPERC)
      IPFLG=1
C
C5------END PRINTOUT WITH TIME SUMMARY AND FORM FEED IF ANY PRINTOUT
C5------WILL BE PRODUCED.
  120 IF(IDDREF.NE.0) THEN
         IF(ASSOCIATED(DDREF,STRT)) THEN
            ALLOCATE(DDREF(NCOL,NROW,NLAY))
            CALL SGWF2BAS7PSV(IGRID)
         END IF
         DDREF=HNEW
         WRITE(IOUT,99)
   99    FORMAT(1X,'Drawdown Reference has been reset to the',
     1               ' end of this time step')
         IDDREF=0
      END IF
      IF(IPFLG.EQ.0) RETURN
      CALL SGWF2BAS7T(KSTP,KPER,DELT,PERTIM,TOTIM,ITMUNI,IOUT)
      WRITE(IOUT,101)
  101 FORMAT('1')
C
C
      WRITE(IOUTPT,) Process.Topology.P
      FORMAT('Process.Topology.P ',)
C
      WRITE(IOUTPT,) Process.Topology.Q
      FORMAT('Process.Topology.Q ',)
C
      WRITE(IOUTPT,) Process.Topology.P
      FORMAT('Process.Topology.P ',)
C
      WRITE(IOUTPT,) ComputationalGrid.Lower.X
      FORMAT('ComputationalGrid.Lower.X ',)
C
      WRITE(IOUTPT,) ComputationalGrid.Lower.Y
      FORMAT('ComputationalGrid.Lower.Y ',)
C
      WRITE(IOUTPT,) ComputationalGrid.Lower.Z
      FORMAT('ComputationalGrid.Lower.Z ',)
C
      WRITE(IOUTPT,) ComputationalGrid.NX
      FORMAT('ComputationalGrid.NX ',)
C
      WRITE(IOUTPT,) ComputationalGrid.NY
      FORMAT('ComputationalGrid.NY ',)
C
      WRITE(IOUTPT,) ComputationalGrid.NZ
      FORMAT('ComputationalGrid.NZ ',)
C
      WRITE(IOUTPT,) GeomInput.Names
      FORMAT('GeomInput.Names ',)
C
      FMTLEN=LEN(geom_input_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geom_input_name GeomInput.geom_input_name.InputType
      FORMAT('pfset GeomInput.',FMT,'.InputType ',)
C  
      FMTLEN=LEN(geom_input_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geom_input_name GeomInput.geom_input_name.GeomNames
      FORMAT('pfset GeomInput.',FMT,'.GeomNames ',)
C  
      FMTLEN=LEN(geom_input_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geom_input_name GeomInput.geom_input_name.Filename
      FORMAT('pfset GeomInput.',FMT,'.Filename ',)
C  
      FMTLEN=LEN(geometry_input_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geometry_input_name GeomInput.geometry_input_name.Value
      FORMAT('pfset GeomInput.',FMT,'.Value ',)
C  
      FMTLEN=LEN(box_geom_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) box_geom_name Geom.box_geom_name.Lower.X
      FORMAT('pfset Geom.',FMT,'.Lower.X ',)
C  
      FMTLEN=LEN(box_geom_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) box_geom_name Geom.box_geom_name.Lower.Y
      FORMAT('pfset Geom.',FMT,'.Lower.Y ',)
C  
      FMTLEN=LEN(box_geom_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) box_geom_name Geom.box_geom_name.Lower.Z
      FORMAT('pfset Geom.',FMT,'.Lower.Z ',)
C  
      FMTLEN=LEN(box_geom_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) box_geom_name Geom.box_geom_name.Upper.X
      FORMAT('pfset Geom.',FMT,'.Upper.X ',)
C  
      FMTLEN=LEN(box_geom_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) box_geom_name Geom.box_geom_name.Upper.Y
      FORMAT('pfset Geom.',FMT,'.Upper.Y ',)
C  
      FMTLEN=LEN(box_geom_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) box_geom_name Geom.box_geom_name.Upper.Z
      FORMAT('pfset Geom.',FMT,'.Upper.Z ',)
C  
      FMTLEN=LEN(geom_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geom_name Geom.geom_name.Patches
      FORMAT('pfset Geom.',FMT,'.Patches ',)
C  
      WRITE(IOUTPT,) TimingInfo.BaseUnit
      FORMAT('TimingInfo.BaseUnit ',)
C
      WRITE(IOUTPT,) TimingInfo.StartCount
      FORMAT('TimingInfo.StartCount ',)
C
      WRITE(IOUTPT,) TimingInfo.StartTime
      FORMAT('TimingInfo.StartTime ',)
C
      WRITE(IOUTPT,) TimingInfo.StopTime
      FORMAT('TimingInfo.StopTime ',)
C
      WRITE(IOUTPT,) TimingInfo.DumpInterval
      FORMAT('TimingInfo.DumpInterval ',)
C
      WRITE(IOUTPT,) TimeStep.Type
      FORMAT('TimeStep.Type ',)
C
      WRITE(IOUTPT,) TimeStep.Value
      FORMAT('TimeStep.Value ',)
C
      WRITE(IOUTPT,) TimeStep.InitialStep
      FORMAT('TimeStep.InitialStep ',)
C
      WRITE(IOUTPT,) TimeStep.GrowthFactor
      FORMAT('TimeStep.GrowthFactor ',)
C
      WRITE(IOUTPT,) TimeStep.MaxStep
      FORMAT('TimeStep.MaxStep ',)
C
      WRITE(IOUTPT,) TimeStep.MinStep
      FORMAT('TimeStep.MinStep ',)
C
      WRITE(IOUTPT,) CycleNames
      FORMAT('CycleNames ',)
C
      FMTLEN=LEN(cycle_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) cycle_name Cycle.cycle_name.Names
      FORMAT('pfset Cycle.',FMT,'.Names ',)
C  
      FMTLEN=LEN(cycle_name)
      FMTLN1=LEN(interval_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(FMT1,'("I", I0, ")")') FMTLN1
      WRITE(IOUTPT,) cycle_name interval_name Cycle.cycle_name.interval_name.Length
      FORMAT('pfset Cycle.',FMT,'.',FMT1,'.Length ',)
C  
      FMTLEN=LEN(cycle_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) cycle_name Cycle.cycle_name.Repeat
      FORMAT('pfset Cycle.',FMT,'.Repeat ',)
C  
      WRITE(IOUTPT,) Domain.GeomName
      FORMAT('Domain.GeomName ',)
C
      WRITE(IOUTPT,) Contaminant.Names
      FORMAT('Contaminant.Names ',)
C
      FMTLEN=LEN(phase_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) phase_name Phase.phase_name.Density.Type
      FORMAT('pfset Phase.',FMT,'.Density.Type ',)
C  
      FMTLEN=LEN(phase_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) phase_name Phase.phase_name.Density.Value
      FORMAT('pfset Phase.',FMT,'.Density.Value ',)
C  
      FMTLEN=LEN(phase_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) phase_name Phase.phase_name.Density.ReferenceDensity
      FORMAT('pfset Phase.',FMT,'.Density.ReferenceDensity ',)
C  
      FMTLEN=LEN(phase_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) phase_name Phase.phase_name.Density.CompressibilityConstant
      FORMAT('pfset Phase.',FMT,'.Density.CompressibilityConstant ',)
C  
      FMTLEN=LEN(phase_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) phase_name Phase.phase_name.Viscosity.Type
      FORMAT('pfset Phase.',FMT,'.Viscosity.Type ',)
C  
      FMTLEN=LEN(phase_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) phase_name Phase.phase_name.Viscosity.Value
      FORMAT('pfset Phase.',FMT,'.Viscosity.Value ',)
C  
      FMTLEN=LEN(contaminant_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) contaminant_name Contaminants.contaminant_name.Degradation.Value
      FORMAT('pfset Contaminants.',FMT,'.Degradation.Value ',)
C  
      WRITE(IOUTPT,) Perm.Conditioning.FileName
      FORMAT('Perm.Conditioning.FileName ',)
C
      WRITE(IOUTPT,) Geom.Perm.Names
      FORMAT('Geom.Perm.Names ',)
C
      FMTLEN=LEN(geometry_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geometry_name Geom.geometry_name.Perm.Type
      FORMAT('pfset Geom.',FMT,'.Perm.Type ',)
C  
      FMTLEN=LEN(geometry_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geometry_name Geom.geometry_name.Perm.Value
      FORMAT('pfset Geom.',FMT,'.Perm.Value ',)
C  
      FMTLEN=LEN(geometry_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geometry_name Geom.geometry_name.Perm.LambdaX
      FORMAT('pfset Geom.',FMT,'.Perm.LambdaX ',)
C  
      FMTLEN=LEN(geometry_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geometry_name Geom.geometry_name.Perm.LambdaY
      FORMAT('pfset Geom.',FMT,'.Perm.LambdaY ',)
C  
      FMTLEN=LEN(geometry_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geometry_name Geom.geometry_name.Perm.LambdaZ
      FORMAT('pfset Geom.',FMT,'.Perm.LambdaZ ',)
C  
      FMTLEN=LEN(geometry_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geometry_name Geom.geometry_name.Perm.GeomMean
      FORMAT('pfset Geom.',FMT,'.Perm.GeomMean ',)
C  
      FMTLEN=LEN(geometry_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geometry_name Geom.geometry_name.Perm.Sigma
      FORMAT('pfset Geom.',FMT,'.Perm.Sigma ',)
C  
      FMTLEN=LEN(geometry_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geometry_name Geom.geometry_name.Perm.Seed
      FORMAT('pfset Geom.',FMT,'.Perm.Seed ',)
C  
      FMTLEN=LEN(geometry_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geometry_name Geom.geometry_name.Perm.NumLines
      FORMAT('pfset Geom.',FMT,'.Perm.NumLines ',)
C  
      FMTLEN=LEN(geometry_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geometry_name Geom.geometry_name.Perm.RZeta
      FORMAT('pfset Geom.',FMT,'.Perm.RZeta ',)
C  
      FMTLEN=LEN(geometry_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geometry_name Geom.geometry_name.Perm.KMax
      FORMAT('pfset Geom.',FMT,'.Perm.KMax ',)
C  
      FMTLEN=LEN(geometry_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geometry_name Geom.geometry_name.Perm.DelK
      FORMAT('pfset Geom.',FMT,'.Perm.DelK ',)
C  
      FMTLEN=LEN(geometry_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geometry_name Geom.geometry_name.Perm.MaxNPts
      FORMAT('pfset Geom.',FMT,'.Perm.MaxNPts ',)
C  
      FMTLEN=LEN(geometry_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geometry_name Geom.geometry_name.Perm.MaxCpts
      FORMAT('pfset Geom.',FMT,'.Perm.MaxCpts ',)
C  
      FMTLEN=LEN(geometry_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geometry_name Geom.geometry_name.Perm.LogNormal
      FORMAT('pfset Geom.',FMT,'.Perm.LogNormal ',)
C  
      FMTLEN=LEN(geometry_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geometry_name Geom.geometry_name.Perm.StratType
      FORMAT('pfset Geom.',FMT,'.Perm.StratType ',)
C  
      FMTLEN=LEN(geometry_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geometry_name Geom.geometry_name.Perm.LowCutoff
      FORMAT('pfset Geom.',FMT,'.Perm.LowCutoff ',)
C  
      FMTLEN=LEN(geometry_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geometry_name Geom.geometry_name.Perm.HighCutoff
      FORMAT('pfset Geom.',FMT,'.Perm.HighCutoff ',)
C  
      FMTLEN=LEN(geometry_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geometry_name Geom.geometry_name.Perm.FileName
      FORMAT('pfset Geom.',FMT,'.Perm.FileName ',)
C  
      WRITE(IOUTPT,) Perm.TensorType
      FORMAT('Perm.TensorType ',)
C
      WRITE(IOUTPT,) Geom.Perm.TensorByGeom.Names
      FORMAT('Geom.Perm.TensorByGeom.Names ',)
C
      FMTLEN=LEN(geometry_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geometry_name Geom.geometry_name.Perm.TensorValX
      FORMAT('pfset Geom.',FMT,'.Perm.TensorValX ',)
C  
      FMTLEN=LEN(geometry_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geometry_name Geom.geometry_name.Perm.TensorValY
      FORMAT('pfset Geom.',FMT,'.Perm.TensorValY ',)
C  
      FMTLEN=LEN(geometry_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geometry_name Geom.geometry_name.Perm.TensorValZ
      FORMAT('pfset Geom.',FMT,'.Perm.TensorValZ ',)
C  
      FMTLEN=LEN(geometry_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geometry_name Geom.geometry_name.Perm.TensorFileX
      FORMAT('pfset Geom.',FMT,'.Perm.TensorFileX ',)
C  
      FMTLEN=LEN(geometry_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geometry_name Geom.geometry_name.Perm.TensorFileY
      FORMAT('pfset Geom.',FMT,'.Perm.TensorFileY ',)
C  
      WRITE(IOUTPT,) Geom.Porosity.GeomNames
      FORMAT('Geom.Porosity.GeomNames ',)
C
      FMTLEN=LEN(geometry_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geometry_name Geom.geometry_name.Porosity.Type
      FORMAT('pfset Geom.',FMT,'.Porosity.Type ',)
C  
      FMTLEN=LEN(geometry_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geometry_name Geom.geometry_name.Porosity.Value
      FORMAT('pfset Geom.',FMT,'.Porosity.Value ',)
C  
      Specific_Storage.GeomNames
      WRITE(IOUTPT,) SpecificStorage.Type
      FORMAT('SpecificStorage.Type ',)
C
      WRITE(IOUTPT,) Solver.Nonlinear.VariableDz
      FORMAT('Solver.Nonlinear.VariableDz ',)
C
      dzScale.GeomNames
      dzScale.Type
      FMTLEN=LEN(geometry_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geometry_name Geom.geometry_name.dzScale.Value
      FORMAT('pfset Geom.',FMT,'.dzScale.Value ',)
C  
      FMTLEN=LEN(geometry_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geometry_name Geom.geometry_name.dzScale.FileName
      FORMAT('pfset Geom.',FMT,'.dzScale.FileName ',)
C  
      dzScale.nzListNumber
      FMTLEN=LEN(nzListNumber)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,85) nzListNumber Cell.nzListNumber.dzScale.Value
   85 FORMAT('pfset Cell.',FMT,'.dzScale.Value ',)
C  FORMAT to be filled in, ParFlow variables names to be replaced
      WRITE(IOUTPT,) Mannings.GeomNames
      FORMAT('Mannings.GeomNames ',)
C
      WRITE(IOUTPT,) Mannings.Type
      FORMAT('Mannings.Type ',)
C
      FMTLEN=LEN(geometry_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geometry_name Mannings.Geom.geometry_name.Value
      FORMAT('pfset Mannings.Geom.',FMT,'.Value ',)
C  
      WRITE(IOUTPT,) Mannings.FileName
      FORMAT('Mannings.FileName ',)
C
      WRITE(IOUTPT,) ToposlopesX.GeomNames
      FORMAT('ToposlopesX.GeomNames ',)
C
      WRITE(IOUTPT,) ToposlopesY.GeomNames
      FORMAT('ToposlopesY.GeomNames ',)
C
      WRITE(IOUTPT,) ToposlopesX.Type
      FORMAT('ToposlopesX.Type ',)
C
      FMTLEN=LEN(geometry_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geometry_name ToposlopeX.Geom.geometry_name.Value
      FORMAT('pfset ToposlopeX.Geom.',FMT,'.Value ',)
C  
      WRITE(IOUTPT,) ToposlopesX.FileName
      FORMAT('ToposlopesX.FileName ',)
C
      WRITE(IOUTPT,) ToposlopesY.FileName
      FORMAT('ToposlopesY.FileName ',)
C
      WRITE(IOUTPT,) Geom.Retardation.GeomNames
      FORMAT('Geom.Retardation.GeomNames ',)
C
      FMTLEN=LEN(geometry_name)
      FMTLN1=LEN(contaminant_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(FMT1,'("I", I0, ")")') FMTLN1
      WRITE(IOUTPT,) geometry_name contaminant_name Geom.geometry_name.contaminant_name.Retardation.Type
      FORMAT('pfset Geom.',FMT,'.',FMT1,'.Retardation.Type ',)
C  
      FMTLEN=LEN(geometry_name)
      FMTLN1=LEN(contaminant_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(FMT1,'("I", I0, ")")') FMTLN1
      WRITE(IOUTPT,) geometry_name contaminant_name Geom.geometry_name.contaminant_name.Retardation.Value
      FORMAT('pfset Geom.',FMT,'.',FMT1,'.Retardation.Value ',)
C  
      FMTLEN=LEN(phase_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) phase_name Phase.phase_name.Mobility.Type
      FORMAT('pfset Phase.',FMT,'.Mobility.Type ',)
C  
      FMTLEN=LEN(phase_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) phase_name Phase.phase_name.Mobility.Value
      FORMAT('pfset Phase.',FMT,'.Mobility.Value ',)
C  
      FMTLEN=LEN(phase_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) phase_name Phase.phase_name.Mobility.Exponent
      FORMAT('pfset Phase.',FMT,'.Mobility.Exponent ',)
C  
      FMTLEN=LEN(phase_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) phase_name Phase.phase_name.Mobility.IrreducibleSaturation
      FORMAT('pfset Phase.',FMT,'.Mobility.IrreducibleSaturation ',)
C  
      WRITE(IOUTPT,) Phase.RelPerm.Type
      FORMAT('Phase.RelPerm.Type ',)
C
      WRITE(IOUTPT,) Phase.RelPerm.GeomNames
      FORMAT('Phase.RelPerm.GeomNames ',)
C
      FMTLEN=LEN(geom_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geom_name Geom.geom_name.RelPerm.Value
      FORMAT('pfset Geom.',FMT,'.RelPerm.Value ',)
C  
      WRITE(IOUTPT,) Phase.RelPerm.VanGenuchten.File
      FORMAT('Phase.RelPerm.VanGenuchten.File ',)
C
      FMTLEN=LEN(geom_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geom_name Geom.geom_name.RelPerm.Alpha.Filename
      FORMAT('pfset Geom.',FMT,'.RelPerm.Alpha.Filename ',)
C  
      FMTLEN=LEN(geom_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geom_name Geom.geom_name.RelPerm.Alpha
      FORMAT('pfset Geom.',FMT,'.RelPerm.Alpha ',)
C  
      FMTLEN=LEN(geom_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geom_name Geom.geom_name.RelPerm.N
      FORMAT('pfset Geom.',FMT,'.RelPerm.N ',)
C  
      FMTLEN=LEN(geom_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geom_name Geom.geom_name.RelPerm.A
      FORMAT('pfset Geom.',FMT,'.RelPerm.A ',)
C  
      FMTLEN=LEN(geom_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geom_name Geom.geom_name.RelPerm.Gamma
      FORMAT('pfset Geom.',FMT,'.RelPerm.Gamma ',)
C  
      FMTLEN=LEN(geom_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geom_name Geom.geom_name.RelPerm.Degree
      FORMAT('pfset Geom.',FMT,'.RelPerm.Degree ',)
C  
      FMTLEN=LEN(phase_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) phase_name PhaseSources.phase_name.Type
      FORMAT('pfset PhaseSources.',FMT,'.Type ',)
C  
      FMTLEN=LEN(phase_name)
      FMTLN1=LEN(geom_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(FMT1,'("I", I0, ")")') FMTLN1
      WRITE(IOUTPT,) phase_name geom_name PhaseSources.phase_name.Geom.geom_name.Value
      FORMAT('pfset PhaseSources.',FMT,'.Geom.',FMT1,'.Value ',)
C  
      FMTLEN=LEN(phase_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) phase_name PhaseSources.phase_name.PredefinedFunction
      FORMAT('pfset PhaseSources.',FMT,'.PredefinedFunction ',)
C  
      FMTLEN=LEN(phase_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) phase_name CapPressure.phase_name.Type
      FORMAT('pfset CapPressure.',FMT,'.Type ',)
C  
      WRITE(IOUTPT,) Phase.Saturation.Type
      FORMAT('Phase.Saturation.Type ',)
C
      WRITE(IOUTPT,) Phase.Saturation.GeomNames
      FORMAT('Phase.Saturation.GeomNames ',)
C
      FMTLEN=LEN(geom_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geom_name Geom.geom_name.Saturation.Value
      FORMAT('pfset Geom.',FMT,'.Saturation.Value ',)
C  
      WRITE(IOUTPT,) Phase.Saturation.VanGenuchten.File
      FORMAT('Phase.Saturation.VanGenuchten.File ',)
C
      FMTLEN=LEN(geom_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geom_name Geom.geom_name.Saturation.Alpha.Filename
      FORMAT('pfset Geom.',FMT,'.Saturation.Alpha.Filename ',)
C  
      FMTLEN=LEN(geom_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geom_name Geom.geom_name.Saturation.N.Filename
      FORMAT('pfset Geom.',FMT,'.Saturation.N.Filename ',)
C  
      FMTLEN=LEN(geom_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geom_name Geom.geom_name.Saturation.SRes.Filename
      FORMAT('pfset Geom.',FMT,'.Saturation.SRes.Filename ',)
C  
      FMTLEN=LEN(geom_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geom_name Geom.geom_name.Saturation.SSat.Filename
      FORMAT('pfset Geom.',FMT,'.Saturation.SSat.Filename ',)
C  
      FMTLEN=LEN(geom_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geom_name Geom.geom_name.Saturation.Alpha
      FORMAT('pfset Geom.',FMT,'.Saturation.Alpha ',)
C  
      FMTLEN=LEN(geom_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geom_name Geom.geom_name.Saturation.N
      FORMAT('pfset Geom.',FMT,'.Saturation.N ',)
C  
      FMTLEN=LEN(geom_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geom_name Geom.geom_name.Saturation.SRes
      FORMAT('pfset Geom.',FMT,'.Saturation.SRes ',)
C  
      FMTLEN=LEN(geom_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geom_name Geom.geom_name.Saturation.SSat
      FORMAT('pfset Geom.',FMT,'.Saturation.SSat ',)
C  
      FMTLEN=LEN(geom_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geom_name Geom.geom_name.Saturation.A
      FORMAT('pfset Geom.',FMT,'.Saturation.A ',)
C  
      FMTLEN=LEN(geom_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geom_name Geom.geom_name.Saturation.Gamma
      FORMAT('pfset Geom.',FMT,'.Saturation.Gamma ',)
C  
      FMTLEN=LEN(geom_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geom_name Geom.geom_name.Saturation.Degree
      FORMAT('pfset Geom.',FMT,'.Saturation.Degree ',)
C  
      Geom.geom_name.Saturation.Coeff.coeff_number
      FMTLEN=LEN(geom_name)
      FMTLN1=LEN(coeff_number)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(FMT1,'("I", I0, ")")') FMTLN1
      WRITE(IOUTPT,) geom_name coeff_number Geom.geom_name.Saturation.Coeff.coeff_number
      FORMAT('pfset Geom.',FMT,'.Saturation.Coeff.',FMT1,' ',)
C  
      FMTLEN=LEN(geom_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geom_name Geom.geom_name.Saturation.FileName
      FORMAT('pfset Geom.',FMT,'.Saturation.FileName ',)
C  
      WRITE(IOUTPT,) InternalBC.Names
      FORMAT('InternalBC.Names ',)
C
      FMTLEN=LEN(internal_bc_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) internal_bc_name InternalBC.internal_bc_name.X
      FORMAT('pfset InternalBC.',FMT,'.X ',)
C  
      FMTLEN=LEN(internal_bc_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) internal_bc_name InternalBC.internal_bc_name.Z
      FORMAT('pfset InternalBC.',FMT,'.Z ',)
C  
      WRITE(IOUTPT,) BCPressure.PatchNames
      FORMAT('BCPressure.PatchNames ',)
C
      FMTLEN=LEN(patch_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) patch_name Patch.patch_name.BCPressure.Type
      FORMAT('pfset Patch.',FMT,'.BCPressure.Type ',)
C  
      FMTLEN=LEN(patch_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) patch_name Patch.patch_name.BCPressure.Cycle
      FORMAT('pfset Patch.',FMT,'.BCPressure.Cycle ',)
C  
      FMTLEN=LEN(patch_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) patch_name Patch.patch_name.BCPressure.RefGeom
      FORMAT('pfset Patch.',FMT,'.BCPressure.RefGeom ',)
C  
      FMTLEN=LEN(patch_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) patch_name Patch.patch_name.BCPressure.RefPatch
      FORMAT('pfset Patch.',FMT,'.BCPressure.RefPatch ',)
C  
      FMTLEN=LEN(patch_name)
      FMTLN1=LEN(interval_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(FMT1,'("I", I0, ")")') FMTLN1
      WRITE(IOUTPT,) patch_name interval_name Patch.patch_name.BCPressure.interval_name.Value
      FORMAT('pfset Patch.',FMT,'.BCPressure.',FMT1,'.Value ',)
C  
      FMTLEN=LEN(patch_name)
      FMTLN1=LEN(interval_name)
      FMTLN2=LEN(phase_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(FMT1,'("I", I0, ")")') FMTLN1
      WRITE(FMT2,'("I", I0, ")")') FMTLN2
      WRITE(IOUTPT,) patch_name interval_name phase_name Patch.patch_name.BCPressure.interval_name.phase_name.IntValue
      FORMAT('pfset Patch.',FMT,'.BCPressure.',FMT1,'.',FMT2,'.IntValue ',)
C  
      FMTLEN=LEN(patch_name)
      FMTLN1=LEN(interval_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(FMT1,'("I", I0, ")")') FMTLN1
      WRITE(IOUTPT,) patch_name interval_name Patch.patch_name.BCPressure.interval_name.XLower
      FORMAT('pfset Patch.',FMT,'.BCPressure.',FMT1,'.XLower ',)
C  
      FMTLEN=LEN(patch_name)
      FMTLN1=LEN(interval_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(FMT1,'("I", I0, ")")') FMTLN1
      WRITE(IOUTPT,) patch_name interval_name Patch.patch_name.BCPressure.interval_name.YLower
      FORMAT('pfset Patch.',FMT,'.BCPressure.',FMT1,'.YLower ',)
C  
      FMTLEN=LEN(patch_name)
      FMTLN1=LEN(interval_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(FMT1,'("I", I0, ")")') FMTLN1
      WRITE(IOUTPT,) patch_name interval_name Patch.patch_name.BCPressure.interval_name.XUpper
      FORMAT('pfset Patch.',FMT,'.BCPressure.',FMT1,'.XUpper ',)
C  
      FMTLEN=LEN(patch_name)
      FMTLN1=LEN(interval_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(FMT1,'("I", I0, ")")') FMTLN1
      WRITE(IOUTPT,) patch_name interval_name Patch.patch_name.BCPressure.interval_name.YUpper
      FORMAT('pfset Patch.',FMT,'.BCPressure.',FMT1,'.YUpper ',)
C  
      FMTLEN=LEN(patch_name)
      FMTLN1=LEN(interval_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(FMT1,'("I", I0, ")")') FMTLN1
      WRITE(IOUTPT,) patch_name interval_name Patch.patch_name.BCPressure.interval_name.NumPoints
      FORMAT('pfset Patch.',FMT,'.BCPressure.',FMT1,'.NumPoints ',)
C  
      FMTLEN=LEN(patch_name)
      FMTLN1=LEN(interval_name)
      FMTLN2=LEN(point_number)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(FMT1,'("I", I0, ")")') FMTLN1
      WRITE(FMT2,'("I", I0, ")")') FMTLN2
      WRITE(IOUTPT,) patch_name interval_name point_number Patch.patch_name.BCPressure.interval_name.point_number.Location
      FORMAT('pfset Patch.',FMT,'.BCPressure.',FMT1,'.',FMT2,'.Location ',)
C  
      FMTLEN=LEN(patch_name)
      FMTLN1=LEN(interval_name)
      FMTLN2=LEN(point_number)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(FMT1,'("I", I0, ")")') FMTLN1
      WRITE(FMT2,'("I", I0, ")")') FMTLN2
      WRITE(IOUTPT,) patch_name interval_name point_number Patch.patch_name.BCPressure.interval_name.point_number.Value
      FORMAT('pfset Patch.',FMT,'.BCPressure.',FMT1,'.',FMT2,'.Value ',)
C  
      FMTLEN=LEN(patch_name)
      FMTLN1=LEN(interval_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(FMT1,'("I", I0, ")")') FMTLN1
      WRITE(IOUTPT,) patch_name interval_name Patch.patch_name.BCPressure.interval_name.FileName
      FORMAT('pfset Patch.',FMT,'.BCPressure.',FMT1,'.FileName ',)
C  
      FMTLEN=LEN(patch_name)
      FMTLN1=LEN(interval_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(FMT1,'("I", I0, ")")') FMTLN1
      WRITE(IOUTPT,) patch_name interval_name Patch.patch_name.BCPressure.interval_name.PredefinedFunction
      FORMAT('pfset Patch.',FMT,'.BCPressure.',FMT1,'.PredefinedFunction ',)
C  
      WRITE(IOUTPT,) BCSaturation.PatchNames
      FORMAT('BCSaturation.PatchNames ',)
C
      FMTLEN=LEN(patch_name)
      FMTLN1=LEN(phase_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(FMT1,'("I", I0, ")")') FMTLN1
      WRITE(IOUTPT,) patch_name phase_name Patch.patch_name.BCSaturation.phase_name.Type
      FORMAT('pfset Patch.',FMT,'.BCSaturation.',FMT1,'.Type ',)
C  
      FMTLEN=LEN(patch_name)
      FMTLN1=LEN(phase_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(FMT1,'("I", I0, ")")') FMTLN1
      WRITE(IOUTPT,) patch_name phase_name Patch.patch_name.BCSaturation.phase_name.Value
      FORMAT('pfset Patch.',FMT,'.BCSaturation.',FMT1,'.Value ',)
C  
      FMTLEN=LEN(patch_name)
      FMTLN1=LEN(phase_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(FMT1,'("I", I0, ")")') FMTLN1
      WRITE(IOUTPT,) patch_name phase_name Patch.patch_name.BCSaturation.phase_name.XLower
      FORMAT('pfset Patch.',FMT,'.BCSaturation.',FMT1,'.XLower ',)
C  
      FMTLEN=LEN(patch_name)
      FMTLN1=LEN(phase_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(FMT1,'("I", I0, ")")') FMTLN1
      WRITE(IOUTPT,) patch_name phase_name Patch.patch_name.BCSaturation.phase_name.YLower
      FORMAT('pfset Patch.',FMT,'.BCSaturation.',FMT1,'.YLower ',)
C  
      FMTLEN=LEN(patch_name)
      FMTLN1=LEN(phase_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(FMT1,'("I", I0, ")")') FMTLN1
      WRITE(IOUTPT,) patch_name phase_name Patch.patch_name.BCSaturation.phase_name.XUpper
      FORMAT('pfset Patch.',FMT,'.BCSaturation.',FMT1,'.XUpper ',)
C  
      FMTLEN=LEN(patch_name)
      FMTLN1=LEN(phase_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(FMT1,'("I", I0, ")")') FMTLN1
      WRITE(IOUTPT,) patch_name phase_name Patch.patch_name.BCSaturation.phase_name.YUpper
      FORMAT('pfset Patch.',FMT,'.BCSaturation.',FMT1,'.YUpper ',)
C  
      FMTLEN=LEN(patch_name)
      FMTLN1=LEN(phase_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(FMT1,'("I", I0, ")")') FMTLN1
      WRITE(IOUTPT,) patch_name phase_name Patch.patch_name.BCPressure.phase_name.NumPoints
      FORMAT('pfset Patch.',FMT,'.BCPressure.',FMT1,'.NumPoints ',)
C  
      FMTLEN=LEN(patch_name)
      FMTLN1=LEN(phase_name)
      FMTLN2=LEN(point_number)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(FMT1,'("I", I0, ")")') FMTLN1
      WRITE(FMT2,'("I", I0, ")")') FMTLN2
      WRITE(IOUTPT,) patch_name phase_name point_number Patch.patch_name.BCPressure.phase_name.point_number.Location
      FORMAT('pfset Patch.',FMT,'.BCPressure.',FMT1,'.',FMT2,'.Location ',)
C  
      FMTLEN=LEN(patch_name)
      FMTLN1=LEN(phase_name)
      FMTLN2=LEN(point_number)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(FMT1,'("I", I0, ")")') FMTLN1
      WRITE(FMT2,'("I", I0, ")")') FMTLN2
      WRITE(IOUTPT,) patch_name phase_name point_number Patch.patch_name.BCPressure.phase_name.point_number.Value
      FORMAT('pfset Patch.',FMT,'.BCPressure.',FMT1,'.',FMT2,'.Value ',)
C  
      FMTLEN=LEN(phase_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) phase_name ICSaturation.phase_name.Type
      FORMAT('pfset ICSaturation.',FMT,'.Type ',)
C  
      FMTLEN=LEN(geom_input_name)
      FMTLN1=LEN(phase_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(FMT1,'("I", I0, ")")') FMTLN1
      WRITE(IOUTPT,) geom_input_name phase_name Geom.geom_input_name.ICSaturation.phase_name.Value
      FORMAT('pfset Geom.',FMT,'.ICSaturation.',FMT1,'.Value ',)
C  
      WRITE(IOUTPT,) ICPressure.Type
      FORMAT('ICPressure.Type ',)
C
      WRITE(IOUTPT,) ICPressure.GeomNames
      FORMAT('ICPressure.GeomNames ',)
C
      FMTLEN=LEN(geom_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geom_name Geom.geom_name.ICPressure.RefElevation
      FORMAT('pfset Geom.',FMT,'.ICPressure.RefElevation ',)
C  
      FMTLEN=LEN(geom_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geom_name Geom.geom_name.ICPressure.RefGeom
      FORMAT('pfset Geom.',FMT,'.ICPressure.RefGeom ',)
C  
      FMTLEN=LEN(geom_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geom_name Geom.geom_name.ICPressure.RefPatch
      FORMAT('pfset Geom.',FMT,'.ICPressure.RefPatch ',)
C  
      FMTLEN=LEN(geom_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) geom_name Geom.geom_name.ICPressure.FileName
      FORMAT('pfset Geom.',FMT,'.ICPressure.FileName ',)
C  
      FMTLEN=LEN(phase_name)
      FMTLN1=LEN(contaminant_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(FMT1,'("I", I0, ")")') FMTLN1
      WRITE(IOUTPT,) phase_name contaminant_name PhaseConcen.phase_name.contaminant_name.Type
      FORMAT('pfset PhaseConcen.',FMT,'.',FMT1,'.Type ',)
C  
      FMTLEN=LEN(phase_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) phase_name PhaseConcen.phase_name.GeomNames
      FORMAT('pfset PhaseConcen.',FMT,'.GeomNames ',)
C  
      FMTLEN=LEN(phase_name)
      FMTLN1=LEN(contaminant_name)
      FMTLN2=LEN(geom_input_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(FMT1,'("I", I0, ")")') FMTLN1
      WRITE(FMT2,'("I", I0, ")")') FMTLN2
      WRITE(IOUTPT,) phase_name contaminant_name geom_input_name PhaseConcen.phase_name.contaminant_name.geom_input_name.Value
      FORMAT('pfset PhaseConcen.',FMT,'.',FMT1,'.',FMT2,'.Value ',)
C  
      FMTLEN=LEN(phase_name)
      FMTLN1=LEN(contaminant_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(FMT1,'("I", I0, ")")') FMTLN1
      WRITE(IOUTPT,) phase_name contaminant_name PhaseConcen.phase_name.contaminant_name.FileName
      FORMAT('pfset PhaseConcen.',FMT,'.',FMT1,'.FileName ',)
C  
      WRITE(IOUTPT,) KnownSolution
      FORMAT('KnownSolution ',)
C
      WRITE(IOUTPT,) Wells.Names
      FORMAT('Wells.Names ',)
C
      FMTLEN=LEN(well_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) well_name Wells.well_name.InputType
      FORMAT('pfset Wells.',FMT,'.InputType ',)
C  
      FMTLEN=LEN(well_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) well_name Wells.well_name.Action
      FORMAT('pfset Wells.',FMT,'.Action ',)
C  
      FMTLEN=LEN(well_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) well_name Wells.well_name.Type
      FORMAT('pfset Wells.',FMT,'.Type ',)
C  
      FMTLEN=LEN(well_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) well_name Wells.well_name.ExtractionType
      FORMAT('pfset Wells.',FMT,'.ExtractionType ',)
C  
      FMTLEN=LEN(well_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) well_name Wells.well_name.InjectionType
      FORMAT('pfset Wells.',FMT,'.InjectionType ',)
C  
      FMTLEN=LEN(well_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) well_name Wells.well_name.X
      FORMAT('pfset Wells.',FMT,'.X ',)
C  
      FMTLEN=LEN(well_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) well_name Wells.well_name.Y
      FORMAT('pfset Wells.',FMT,'.Y ',)
C  
      FMTLEN=LEN(well_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) well_name Wells.well_name.ZUpper
      FORMAT('pfset Wells.',FMT,'.ZUpper ',)
C  
      FMTLEN=LEN(well_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) well_name Wells.well_name.ExtractionZUpper
      FORMAT('pfset Wells.',FMT,'.ExtractionZUpper ',)
C  
      FMTLEN=LEN(well_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) well_name Wells.well_name.InjectionZUpper
      FORMAT('pfset Wells.',FMT,'.InjectionZUpper ',)
C  
      FMTLEN=LEN(well_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) well_name Wells.well_name.ZLower
      FORMAT('pfset Wells.',FMT,'.ZLower ',)
C  
      FMTLEN=LEN(well_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) well_name Wells.well_name.ExtractionZLower
      FORMAT('pfset Wells.',FMT,'.ExtractionZLower ',)
C  
      FMTLEN=LEN(well_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) well_name Wells.well_name.InjectionZLower
      FORMAT('pfset Wells.',FMT,'.InjectionZLower ',)
C  
      FMTLEN=LEN(well_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) well_name Wells.well_name.Method
      FORMAT('pfset Wells.',FMT,'.Method ',)
C  
      FMTLEN=LEN(well_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) well_name Wells.well_name.ExtractionMethod
      FORMAT('pfset Wells.',FMT,'.ExtractionMethod ',)
C  
      FMTLEN=LEN(well_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) well_name Wells.well_name.InjectionMethod
      FORMAT('pfset Wells.',FMT,'.InjectionMethod ',)
C  
      FMTLEN=LEN(well_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) well_name Wells.well_name.Cycle
      FORMAT('pfset Wells.',FMT,'.Cycle ',)
C  
      FMTLEN=LEN(well_name)
      FMTLN1=LEN(interval_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(FMT1,'("I", I0, ")")') FMTLN1
      WRITE(IOUTPT,) well_name interval_name Wells.well_name.interval_name.Pressure.Value
      FORMAT('pfset Wells.',FMT,'.',FMT1,'.Pressure.Value ',)
C  
      FMTLEN=LEN(well_name)
      FMTLN1=LEN(interval_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(FMT1,'("I", I0, ")")') FMTLN1
      WRITE(IOUTPT,) well_name interval_name Wells.well_name.interval_name.Extraction.Pressure.Value
      FORMAT('pfset Wells.',FMT,'.',FMT1,'.Extraction.Pressure.Value ',)
C  
      FMTLEN=LEN(well_name)
      FMTLN1=LEN(interval_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(FMT1,'("I", I0, ")")') FMTLN1
      WRITE(IOUTPT,) well_name interval_name Wells.well_name.interval_name.Injection.Pressure.Value
      FORMAT('pfset Wells.',FMT,'.',FMT1,'.Injection.Pressure.Value ',)
C  
      FMTLEN=LEN(well_name)
      FMTLN1=LEN(interval_name)
      FMTLN2=LEN(phase_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(FMT1,'("I", I0, ")")') FMTLN1
      WRITE(FMT2,'("I", I0, ")")') FMTLN2
      WRITE(IOUTPT,) well_name interval_name phase_name Wells.well_name.interval_name.Flux.phase_name.Value
      FORMAT('pfset Wells.',FMT,'.',FMT1,'.Flux.',FMT2,'.Value ',)
C  
      FMTLEN=LEN(well_name)
      FMTLN1=LEN(interval_name)
      FMTLN2=LEN(phase_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(FMT1,'("I", I0, ")")') FMTLN1
      WRITE(FMT2,'("I", I0, ")")') FMTLN2
      WRITE(IOUTPT,) well_name interval_name phase_name Wells.well_name.interval_name.Extraction.Flux.phase_name.Value
      FORMAT('pfset Wells.',FMT,'.',FMT1,'.Extraction.Flux.',FMT2,'.Value ',)
C  
      FMTLEN=LEN(well_name)
      FMTLN1=LEN(interval_name)
      FMTLN2=LEN(phase_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(FMT1,'("I", I0, ")")') FMTLN1
      WRITE(FMT2,'("I", I0, ")")') FMTLN2
      WRITE(IOUTPT,) well_name interval_name phase_name Wells.well_name.interval_name.Injection.Flux.phase_name.Value
      FORMAT('pfset Wells.',FMT,'.',FMT1,'.Injection.Flux.',FMT2,'.Value ',)
C  
      FMTLEN=LEN(well_name)
      FMTLN1=LEN(interval_name)
      FMTLN2=LEN(phase_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(FMT1,'("I", I0, ")")') FMTLN1
      WRITE(FMT2,'("I", I0, ")")') FMTLN2
      WRITE(IOUTPT,) well_name interval_name phase_name Wells.well_name.interval_name.Saturation.phase_name.Value
      FORMAT('pfset Wells.',FMT,'.',FMT1,'.Saturation.',FMT2,'.Value ',)
C  
      FMTLEN=LEN(well_name)
      FMTLN1=LEN(interval_name)
      FMTLN2=LEN(phase_name)
      FMTLN3=LEN(contaminant_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(FMT1,'("I", I0, ")")') FMTLN1
      WRITE(FMT2,'("I", I0, ")")') FMTLN2
      WRITE(FMT3,'("I", I0, ")")') FMTLN3
      WRITE(IOUTPT,) well_name interval_name phase_name contaminant_name Wells.well_name.interval_name.Concentration.phase_name.contaminant_name.Value
      FORMAT('pfset Wells.',FMT,'.',FMT1,'.Concentration.',FMT2,'.',FMT3,'.Value ',)
C
      FMTLEN=LEN(well_name)
      FMTLN1=LEN(interval_name)
      FMTLN2=LEN(phase_name)
      FMTLN3=LEN(contaminant_name)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(FMT1,'("I", I0, ")")') FMTLN1
      WRITE(FMT2,'("I", I0, ")")') FMTLN2
      WRITE(FMT3,'("I", I0, ")")') FMTLN3
      WRITE(IOUTPT,) well_name interval_name phase_name contaminant_name Wells.well_name.interval_name.Injection.Concentration.phase_name.contaminant_name.Value
      FORMAT('pfset Wells.',FMT,'.',FMT1,'.Injection.Concentration.',FMT2,'.',FMT3,'.Value ',)
C
      WRITE(IOUTPT,) Solver.Linear
      FORMAT('Solver.Linear ',)
C
      WRITE(IOUTPT,) Solver.SadvectOrder
      FORMAT('Solver.SadvectOrder ',)
C
      WRITE(IOUTPT,) Solver.CFL
      FORMAT('Solver.CFL ',)
C
      WRITE(IOUTPT,) Solver.MaxIter
      FORMAT('Solver.MaxIter ',)
C
      WRITE(IOUTPT,) Solver.RelTol
      FORMAT('Solver.RelTol ',)
C
      WRITE(IOUTPT,) Solver.AbsTol
      FORMAT('Solver.AbsTol ',)
C
      WRITE(IOUTPT,) Solver.Drop
      FORMAT('Solver.Drop ',)
C
      WRITE(IOUTPT,) Solver.PrintSubsurf
      FORMAT('Solver.PrintSubsurf ',)
C
      WRITE(IOUTPT,) Solver.PrintPressure
      FORMAT('Solver.PrintPressure ',)
C
      WRITE(IOUTPT,) Solver.PrintVelocities
      FORMAT('Solver.PrintVelocities ',)
C
      WRITE(IOUTPT,) Solver.PrintWells
      FORMAT('Solver.PrintWells ',)
C
      WRITE(IOUTPT,) Solver.PrintLSMSink
      FORMAT('Solver.PrintLSMSink ',)
C
      WRITE(IOUTPT,) Solver.WriteSiloSubsurfData
      FORMAT('Solver.WriteSiloSubsurfData ',)
C
      WRITE(IOUTPT,) Solver.WriteSiloPressure
      FORMAT('Solver.WriteSiloPressure ',)
C
      WRITE(IOUTPT,) Solver.WriteSiloSaturation
      FORMAT('Solver.WriteSiloSaturation ',)
C
      WRITE(IOUTPT,) Solver.WriteSiloConcentration
      FORMAT('Solver.WriteSiloConcentration ',)
C
      WRITE(IOUTPT,) Solver.WriteSiloVelocities
      FORMAT('Solver.WriteSiloVelocities ',)
C
      WRITE(IOUTPT,) Solver.WriteSiloSlopes
      FORMAT('Solver.WriteSiloSlopes ',)
C
      WRITE(IOUTPT,) Solver.WriteSiloMannings
      FORMAT('Solver.WriteSiloMannings ',)
C
      WRITE(IOUTPT,) Solver.WriteSiloMask
      FORMAT('Solver.WriteSiloMask ',)
C
      WRITE(IOUTPT,) Solver.WriteSiloEvapTrans
      FORMAT('Solver.WriteSiloEvapTrans ',)
C
      WRITE(IOUTPT,) Solver.WriteSiloEvapTransSum
      FORMAT('Solver.WriteSiloEvapTransSum ',)
C
      WRITE(IOUTPT,) Solver.WriteSiloOverlandSum
      FORMAT('Solver.WriteSiloOverlandSum ',)
C
      WRITE(IOUTPT,) SILO.Filetype
      FORMAT('SILO.Filetype ',)
C
      WRITE(IOUTPT,) SILO.CompressionOptions
      FORMAT('SILO.CompressionOptions ',)
C
      WRITE(IOUTPT,) Solver.Nonlinear.StepTol
      FORMAT('Solver.Nonlinear.StepTol ',)
C
      WRITE(IOUTPT,) Solver.Nonlinear.MaxIter
      FORMAT('Solver.Nonlinear.MaxIter ',)
C
      WRITE(IOUTPT,) Solver.Linear.KrylovDimension
      FORMAT('Solver.Linear.KrylovDimension ',)
C
      WRITE(IOUTPT,) Solver.Linear.MaxRestarts
      FORMAT('Solver.Linear.MaxRestarts ',)
C
      WRITE(IOUTPT,) Solver.MaxConvergencFailures
      FORMAT('Solver.MaxConvergencFailures ',)
C
      WRITE(IOUTPT,) Solver.Nonlinear.PrintFlag
      FORMAT('Solver.Nonlinear.PrintFlag ',)
C
      WRITE(IOUTPT,) Solver.Nonlinear.EtaChoice
      FORMAT('Solver.Nonlinear.EtaChoice ',)
C
      WRITE(IOUTPT,) Solver.Nonlinear.EtaValue
      FORMAT('Solver.Nonlinear.EtaValue ',)
C
      WRITE(IOUTPT,) Solver.Nonlinear.EtaAlpha
      FORMAT('Solver.Nonlinear.EtaAlpha ',)
C
      WRITE(IOUTPT,) Solver.Nonlinear.EtaGamma
      FORMAT('Solver.Nonlinear.EtaGamma ',)
C
      WRITE(IOUTPT,) Solver.Nonlinear.UseJacobian
      FORMAT('Solver.Nonlinear.UseJacobian ',)
C
      WRITE(IOUTPT,) Solver.Nonlinear.DerivativeEpsilon
      FORMAT('Solver.Nonlinear.DerivativeEpsilon ',)
C
      WRITE(IOUTPT,) Solver.Nonlinear.Globalization
      FORMAT('Solver.Nonlinear.Globalization ',)
C
      WRITE(IOUTPT,) Solver.Linear.Preconditioner
      FORMAT('Solver.Linear.Preconditioner ',)
C
      WRITE(IOUTPT,) Solver.Linear.Preconditioner.SymmetricMat
      FORMAT('Solver.Linear.Preconditioner.SymmetricMat ',)
C
      FMTLEN=LEN(precond_method)
      WRITE(FMT,'("I", I0, ")")') FMTLEN
      WRITE(IOUTPT,) precond_method Solver.Linear.Preconditioner.precond_method.MaxIter
      FORMAT('pfset Solver.Linear.Preconditioner.',FMT,'.MaxIter ',)
C  
      WRITE(IOUTPT,) Solver.Linear.Preconditioner.SMG.NumPreRelax
      FORMAT('Solver.Linear.Preconditioner.SMG.NumPreRelax ',)
C
      WRITE(IOUTPT,) Solver.Linear.Preconditioner.SMG.NumPostRelax
      FORMAT('Solver.Linear.Preconditioner.SMG.NumPostRelax ',)
C
      WRITE(IOUTPT,) Solver.EvapTrans.FileName
      FORMAT('Solver.EvapTrans.FileName ',)
C
      WRITE(IOUTPT,) Solver.LSM
      FORMAT('Solver.LSM ',)
C
      WRITE(IOUTPT,) OverlandFlowSpinUp
      FORMAT('OverlandFlowSpinUp ',)
C
      WRITE(IOUTPT,) OverlandFlowSpinUpDampP1
      FORMAT('OverlandFlowSpinUpDampP1 ',)
C
      WRITE(IOUTPT,) OverlandFlowSpinUpDampP2
      FORMAT('OverlandFlowSpinUpDampP2 ',)
C
      WRITE(IOUTPT,) Solver.CLM.Print1dOut
      FORMAT('Solver.CLM.Print1dOut ',)
C
      WRITE(IOUTPT,) Solver.CLM.IstepStart
      FORMAT('Solver.CLM.IstepStart ',)
C
      WRITE(IOUTPT,) Solver.CLM.MetForcing
      FORMAT('Solver.CLM.MetForcing ',)
C
      WRITE(IOUTPT,) Solver.CLM.MetFilePath
      FORMAT('Solver.CLM.MetFilePath ',)
C
      WRITE(IOUTPT,) Solver.CLM.MetFileNT
      FORMAT('Solver.CLM.MetFileNT ',)
C
      WRITE(IOUTPT,) Solver.CLM.ForceVegetation
      FORMAT('Solver.CLM.ForceVegetation ',)
C
      WRITE(IOUTPT,) Solver.WriteSiloCLM
      FORMAT('Solver.WriteSiloCLM ',)
C
      WRITE(IOUTPT,) Solver.PrintCLM
      FORMAT('Solver.PrintCLM ',)
C
      WRITE(IOUTPT,) Solver.WriteCLMBinary
      FORMAT('Solver.WriteCLMBinary ',)
C
      WRITE(IOUTPT,) Solver.CLM.BinaryOutDir
      FORMAT('Solver.CLM.BinaryOutDir ',)
C
      WRITE(IOUTPT,) Solver.CLM.CLMFileDir
      FORMAT('Solver.CLM.CLMFileDir ',)
C
      WRITE(IOUTPT,) Solver.CLM.CLMDumpInterval
      FORMAT('Solver.CLM.CLMDumpInterval ',)
C
      WRITE(IOUTPT,) Solver.CLM.EvapBeta
      FORMAT('Solver.CLM.EvapBeta ',)
C
      WRITE(IOUTPT,) Solver.CLM.ResSat
      FORMAT('Solver.CLM.ResSat ',)
C
      WRITE(IOUTPT,) Solver.CLM.VegWaterStress
      FORMAT('Solver.CLM.VegWaterStress ',)
C
      WRITE(IOUTPT,) Solver.CLM.WiltingPoint
      FORMAT('Solver.CLM.WiltingPoint ',)
C
      WRITE(IOUTPT,) Solver.CLM.FieldCapacity
      FORMAT('Solver.CLM.FieldCapacity ',)
C
      WRITE(IOUTPT,) Solver.CLM.IrrigationTypes
      FORMAT('Solver.CLM.IrrigationTypes ',)
C
      WRITE(IOUTPT,) Solver.CLM.IrrigationCycle
      FORMAT('Solver.CLM.IrrigationCycle ',)
C
      WRITE(IOUTPT,) Solver.CLM.IrrigationRate
      FORMAT('Solver.CLM.IrrigationRate ',)
C
      WRITE(IOUTPT,) Solver.CLM.IrrigationStartTime
      FORMAT('Solver.CLM.IrrigationStartTime ',)
C
      WRITE(IOUTPT,) Solver.CLM.IrrigationStopTime
      FORMAT('Solver.CLM.IrrigationStopTime ',)
C
      WRITE(IOUTPT,) Solver.CLM.IrrigationThreshold
      FORMAT('Solver.CLM.IrrigationThreshold ',)
C
      WRITE(IOUTPT,) Solver.CLM.ReuseCount
      FORMAT('Solver.CLM.ReuseCount ',)
C
      WRITE(IOUTPT,) Solver.CLM.WriteLogs
      FORMAT('Solver.CLM.WriteLogs ',)
C
      WRITE(IOUTPT,) Solver.CLM.WriteLastRST
      FORMAT('Solver.CLM.WriteLastRST ',)
C
      WRITE(IOUTPT,) Solver.CLM.DailyRST
      FORMAT('Solver.CLM.DailyRST ',)
C
      WRITE(IOUTPT,) Solver.CLM.SingleFile
      FORMAT('Solver.CLM.SingleFile ',)
C
      WRITE(IOUTPT,) Solver.CLM.RootZoneNZ
      FORMAT('Solver.CLM.RootZoneNZ ',)
C
      WRITE(IOUTPT,) Solver.CLM.SoiLayer
      FORMAT('Solver.CLM.SoiLayer ',)
C
C
C6------RETURN
      RETURN
      END
