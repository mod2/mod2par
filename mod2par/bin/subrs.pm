package subrs;
use strict;
use warnings;
use open			qw< :encoding(UTF-8) >;

use POSIX;
use Exporter;
use List::MoreUtils	qw( first_index );

our @ISA		= qw( Exporter );
our @EXPORT_OK	= qw( Fread nono );
our @EXPORT		= qw( Fread nono );

sub Fread {
	my ( @leadcnt,@trailcnt );
	my $handle	= undef;
	my $index  	= first_index { $_ eq $_[0] } @{$_[1]}[0..$#{$_[1]}];
	my $file	= $_[3].${$_[2]}[$index];
	open($handle, "<", $file)
		|| die "$0: can't open $file for reading: $!";
		my @lines  = <$handle>;
	close($handle)
		|| die "$file: $!";
	$handle		= undef;
	foreach ( @lines ) {
		my $leadstr		= /^(\s*)/;
		my $lead			= length( $leadstr );
		push ( @leadcnt, $lead );
		my $trailstr	= /(\s*)$/;
		my $trail			= length( $trailstr );
		push ( @trailcnt, $trail );
	}
	s{^\s*}{}g foreach @lines;
	return ( \@lines,\@leadcnt,\@trailcnt );
}

sub nono {
	my ( $l );
	for ( my $l = $_[0]; $l<=$#{$_[1]}; $l++) {
		die "Error: # misplaced (cf 8-12)"
			unless ( ${$_[1]}[$l]=~/^(?!#)/ );
	}
}

1;
