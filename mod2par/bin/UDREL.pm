package UDREL;
# IPRN (cf 8-57) still unused
use strict;
use warnings;
use open			qw< :encoding(UTF-8) >;

use POSIX;
use Exporter;
use List::MoreUtils	qw( first_index );
use Data::Types		qw( is_float is_real is_decimal is_int is_count is_whole
						to_float to_real to_decimal to_int to_count to_whole );
use Fortran::Format;
use Text::ParseWords;

our @ISA			= qw( Exporter );
our @EXPORT_OK		= qw( UDREL );
our @EXPORT			= qw( UDREL );

sub UDREL {
	my ( @CTRLLINE,$Nunit,$Fname,@Array,$idx,$handle,$String,$FMTIN,$CNSTNT,$i );
	$i			= 1;
	$handle		= undef;
#	@CTRLLINE	= ${$_[0]}[$_[4]]
#				=~ /(?:(CONSTANT)(?:\s*,\s*|\s+)'?(.+)'?)
#					|(?:(INTERNAL)(?:(?:\s*,\s*|\s+)'?(.+)'?){3})
#					|(?:(EXTERNAL)(?:(?:\s*,\s*|\s+)'?(.+)'?){4})
#					|(?:(OPEN\/CLOSE)(?:(?:\s*,\s*|\s+)'?(.+)'?){4})
#					|(?:(.{10}){2}(.{20})(.{10}))/i;
	@CTRLLINE	= quotewords( '\s*,\s*|\s+', 0, ${$_[0]}[$_[4]] );
	if ( $CTRLLINE[0]=~/CONSTANT/i ) {
		die "Error: UDREL CONSTANT - Incorrect type (cf 8-57)"
			unless ( is_float($CTRLLINE[1])
					|| is_real($CTRLLINE[1])
					|| is_decimal($CTRLLINE[1]) );
		@Array	= ($CTRLLINE[1] x $_[3]);
		return ( \@Array,$_[4]+$i-1 );
	}
	elsif ( $CTRLLINE[0]=~/INTERNAL/i ) {
		die "Error: UDREL INTERNAL - Incorrect type (cf 8-57)"
			unless ( ( is_float($CTRLLINE[1])
						|| is_real($CTRLLINE[1])
						|| is_decimal($CTRLLINE[1]) )
					&& ( length($CTRLLINE[2]) <= 20 )
					&& ( is_int($CTRLLINE[3])
						|| is_count($CTRLLINE[3])
						|| is_whole($CTRLLINE[3]) ) );
		$String	= @{$_[0]}[$_[4]+$i];
		if ( $CTRLLINE[3]=~/FREE/i ) {
#			@Array	= $String =~ /'?(.+)'?(?:(?:\s*,\s*|\s+)'?(.+)'?){$_[3]-1}/;
			@Array	= quotewords( '\s*,\s*|\s+', 0, $String );
		}
		elsif ( $CTRLLINE[3]=~/BINARY/i ) {
# UNIMPLEMENTED
		...;
		}
		else {
			$FMTIN	= Fortran::Format	-> new($CTRLLINE[3]);
			@Array	= $FMTIN			-> read( $String, $_[3] );
		}
		while ( $#Array<=$_[3]-1 ) {
			$i++;
			$String	= join( '\n', $String,${$_[0]}[$_[4]+$i] );
			if ( $CTRLLINE[3]=~/FREE/i ) {
#				@Array	= $String =~ /'?(.+)'?(?:(?:\s*,\s*|\s+)'?(.+)'?){$_[3]-1}/;
				@Array	= quotewords( '\s*,\s*|\s+', 0, $String );
			}
			elsif ( $CTRLLINE[3]=~/BINARY/i ) {
# UNIMPLEMENTED
			...;
			}
			else {
				$FMTIN	= Fortran::Format	-> new($CTRLLINE[3]);
				@Array	= $FMTIN			-> read( $String, $_[3] );
			}
		}
		foreach $CNSTNT ( @Array ) {
			$CNSTNT	= $CNSTNT * $CTRLLINE[2];
		}
		return ( \@Array,$_[4]+$i-1 );
	}
	elsif ( $CTRLLINE[0]=~/EXTERNAL/i ) {
		die "Error: UDREL EXTERNAL - Incorrect type cf(8-57)"
			unless ( ( is_int($CTRLLINE[1])
						|| is_count($CTRLLINE[1])
						|| is_whole($CTRLLINE[1]) )
					&& ( is_float($CTRLLINE[2])
						|| is_real($CTRLLINE[2])
						|| is_decimal($CTRLLINE[2]) )
					&& ( length($CTRLLINE[3]) <= 20 )
					&& ( is_int($CTRLLINE[4])
						|| is_count($CTRLLINE[4])
						|| is_whole($CTRLLINE[4]) ) );
		foreach $Nunit (@{$_[1]}[0..$#{$_[1]}]) {
			die "Error: UDREL EXTERNAL - Nunit missing from NAM (cf 8-60)"
				unless ( $Nunit==$CTRLLINE[1] );
		}
		$idx	= first_index { /$CTRLLINE[1]/ } @{$_[1]}[0..$#{$_[1]}];
		open($handle, "<", ${$_[2]}[$idx])
			|| die "$0: can't open ${$_[2]}[$idx] for reading: $!";
			$String	= <$handle>;
		close($handle)
			|| die "${$_[2]}[$idx]: $!";
		$handle	= undef;
		if ( $CTRLLINE[3]=~/FREE/i ) {
#			@Array	= $String =~ /'?(.+)'?(?:(?:\s*,\s*|\s+)'?(.+)'?){$_[3]-1}/;
			@Array	= quotewords( '\s*,\s*|\s+', 0, $String );
		}
		elsif ( $CTRLLINE[3]=~/BINARY/i ) {
# UNIMPLEMENTED
		...;
		}
		else {
			$FMTIN	= Fortran::Format	-> new($CTRLLINE[3]);
			@Array	= $FMTIN			-> read( $String, $_[3] );
		}
		foreach $CNSTNT ( @Array ) {
			$CNSTNT	= $CNSTNT * $CTRLLINE[2];
		}
		return ( \@Array,$_[4]+$i-1 );
	}
	elsif ( $CTRLLINE[0]=~/OPEN\/CLOSE/i ) {
		foreach $Fname (@{$_[2]}[0..$#{$_[2]}]) {
			die "Error: UDREL OPEN/CLOSE - FNAME match in NAM (cf 8-60)"
				unless ( $Fname!=$CTRLLINE[1] );
		}
		die "Error: UDREL OPEN/CLOSE - Incorrect type (cf 8-57)"
			unless ( ( is_float($CTRLLINE[2])
						|| is_real($CTRLLINE[2])
						|| is_decimal($CTRLLINE[2]) )
					&& ( length($CTRLLINE[3]) <= 20 )
					&& ( is_int($CTRLLINE[4])
						|| is_count($CTRLLINE[4])
						|| is_whole($CTRLLINE[4]) ) );
		open($handle, "<", $CTRLLINE[1])
			|| die "$0: can't open $CTRLLINE[1] for reading: $!";
			$String	= <$handle>;
		close($handle)
			|| die "$CTRLLINE[1]: $!";
		$handle	= undef;
		if ( $CTRLLINE[3]=~/FREE/i ) {
#			@Array	= $String =~ /'?(.+)'?(?:(?:\s*,\s*|\s+)'?(.+)'?){$_[3]-1}/;
			@Array	= quotewords( '\s*,\s*|\s+', 0, $String );
		}
		elsif ( $CTRLLINE[3]=~/BINARY/i ) {
# UNIMPLEMENTED
		...;
		}
		else {
			$FMTIN	= Fortran::Format	-> new($CTRLLINE[3]);
			@Array	= $FMTIN			-> read( $String, $_[3] );
		}
		foreach $CNSTNT ( @Array ) {
			$CNSTNT	= $CNSTNT * $CTRLLINE[2];
		}
		return ( \@Array,$_[4]+$i-1 );
	}
	else {
		@CTRLLINE	= ${$_[0]}[$_[4]]
					=~ /(.{10-\Q${$_[5]}[$_[4]]})(.{10})(.{20})(.{10-\Q${$_[6]}[$_[4]]})/i;
		die "Error: UDREL FIXED - Incorrect type (cf 8-57)"
			unless ( ( is_int(to_int($CTRLLINE[0])) 
						|| is_count(to_count($CTRLLINE[0]))
						|| is_whole(is_whole($CTRLLINE[0])) )
					&& ( is_float(to_float($CTRLLINE[1]))
						|| is_real(to_real($CTRLLINE[1]))
						|| is_decimal(to_decimal($CTRLLINE[1])) )
					&& ( is_int(to_int($CTRLLINE[3]))
						|| is_count(to_count($CTRLLINE[3]))
						|| is_whole(to_whole($CTRLLINE[3])) ) );
# UNIMPLEMENTED: LOCAT?
		...;
	}
}

1;
