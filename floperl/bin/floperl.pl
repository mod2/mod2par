#!/usr/bin/perl
# All must be updated to reflect MODFLOW-2005
# Counters - i,j
use strict;
use warnings;
use open					qw< :encoding(UTF-8) >;

use POSIX;
use List::MoreUtils			qw( true first_index );
use List::Util				1.33 'any';
use Data::Types				qw( is_int is_count is_whole );
use Text::ParseWords;
use Sys::CPU;
use UDREL;
use U2DINT;
use subrs;

my ( @Ftypes,@Nunits,@Fnames,@Fstatuses,$Ftype,$lines,@lines,$line,
	$NLAY,$NROW,$NCOL,$NPER,$ITMUNI,$LENUNI,$j,$k,@Options,$NCxR,$NCxL,
	$leadcnt,@leadcnt,$trailcnt,@trailcnt,$path,$handle );
my $in					= "../share/in/";
my $out					= "../share/out/";
my $nam					= "str.nam";
my $prefix				= $nam
						=~ /^(\S+).nam/;


#####
# NAM
#####

NAMread: {
	$path				= $in.$nam;
	$handle				= undef;
	open($handle,"<",$path)
		|| die "$0: can't open $path for reading: $!";
		@lines	= <$handle>;
	close($handle)
		|| die "$path: $!";
	$handle				= undef;
}

# NAM is parsed into an array of files to decipher
NAMasn: {
	my $i				= 0;
	NAMAsn: foreach $line (@lines) {
		my @namestmp;
		# FREE format regexp for re-use
		if ( length($line)<300 && $line=~/^(?!#)/ && $i==0) {
			@namestmp	= quotewords( '\s*,\s*|\s+', 0, $line );
			@namestmp	= grep { defined && /\S/ } @namestmp;
#			@namestmp	= $line
#						=~ /'?(.+)'?(?:(?:\s*,\s*|\s+)'?(.+)'?){2,3}/;
#						=~ /^'?(\S+)'?(?:\s*,\s*|\s+)'?(\S+)'?(?:\s*,\s*|\s+)'?(\S+)'?(?:(?:\s*,\s*|\s+)'?(\S+)'?)?$/;
			die "Error: LIST file not firstly listed within NAM (cf 8-10)"
				unless ($namestmp[0]=~/^LIST$/i);
			$i++;
	# Single dimensional representatives of multi-dimensional arrays throughout
	#		$names[$i]	= [ @namestmp ];
			push (@Ftypes, $namestmp[0]);
# Nunits assigned Fnames
			push (@Nunits, $namestmp[1]);
			push (@Fnames, $namestmp[2]);
			if ($#namestmp==3) {
				push (@Fstatuses, $namestmp[3])
			}
			else {
				push (@Fstatuses, "")
			}
		}
		elsif ( length($line)<300 && $line=~/^(?!#)/ ) {
			@namestmp	= quotewords( '\s*,\s*|\s+', 0, $line );
			@namestmp	= grep { defined && /\S/ } @namestmp;
#			@namestmp	= $line
#						=~ /'?(.+)'?(?:(?:\s*,\s*|\s+)'?(.+)'?){2,3}/;
#						=~ /^'?(\S+)'?(?:\s*,\s*|\s+)'?(\S+)'?(?:\s*,\s*|\s+)'?(\S+)'?(?:(?:\s*,\s*|\s+)'?(\S+)'?)?$/;
			$i++;
			push (@Ftypes, $namestmp[0]);
			push (@Nunits, $namestmp[1]);
			push (@Fnames, $namestmp[2]);
			if ($#namestmp==3) {
				push (@Fstatuses, $namestmp[3])
			}
			else {
				push (@Fstatuses, "")
			}
		}
		elsif ( $line=~/^#/ ) {
		}
		else {
			die "Error: NAM - line longer than permitted (cf 8-10)";
		}
	}
}

NAMerr: {

	# Type or number of files appropriate
	NAMFtype: {
		die "Error: Missing one of files NAM, DIS, or BAS6 (cf 8-10)"
			unless ( ( any { /^DIS$/i } @Ftypes[0..$#Ftypes] )
					&& ( any { /^BAS6$/i } @Ftypes[0..$#Ftypes] ) );
		NAMFtypelo: foreach $Ftype (@Ftypes[0..$#Ftypes]) {
			my $FtypesCnt	= true { /^\Q$Ftype/ } @Ftypes[0..$#Ftypes];
			die "Error: Incorrect type or number of files (cf 8-10)"
				unless( ( $Ftype=~/^(?:LIST|DIS|BAS6|MULT|ZONE|OC|CHD|PVAL|BCF6|LPF|HFB6|RCH|RIV|WEL|DRN|GHB|EVT|SIP|PCG|DE4|DATA|DATA\(BINARY\)|STR)/i )
					&& ( $FtypesCnt==1 || /^DATA/i ) );
		}
	}
	
	## Nunit
	NAMNunit: {
		NAMNunitNaN: foreach my $Nunit (@Nunits[0..$#Nunits]) {
			die "Error: Nunit not valid, numeric (cf 8-11)"
				unless ( ( is_int($Nunits[0]) 
							|| is_count($Nunits[0])
							|| is_whole($Nunits[0]) )
						&& ( $Nunit!=99 ) );
		}
		my %seen		= ();
		my @NunitsUnq	= grep { ! $seen{$_} ++ } @Nunits;
		die "Error: Nunit not unique (cf 8-11)"
			unless ( $#NunitsUnq==$#Nunits );
	}
	
	## Fname
	NAMFname: foreach my $Fname (@Fnames[0..$#Fnames]) {
		die "Error: Fname not file-like (cf 8-11)"
			unless ( $Fname=~/[\w-]\.[\w-]/i );
	}
	
	## Fstatus
	NAMFstatus: foreach my $Fstatus (@Fstatuses[0..$#Fstatuses]) {
		if ( length($Fstatus) ) {
			die "Error: Fstatus not tolerable value (cf 8-11)"
				unless ( $Fstatus=~/^(?:OLD|REPLACE)$|/i );
		}
	}
	
}

#####


#####
# DIS
#####

DISread: {
	$Ftype				= 'DIS';
	( $lines,$leadcnt,$trailcnt )	= Fread( $Ftype,\@Ftypes,\@Fnames,$in );
	@lines							= @$lines;
	@leadcnt						= @$leadcnt;
	@trailcnt						= @$trailcnt;
}

DISasn: {

	$j					= 0;
	my @LAYCBD;

	DIS1: foreach my $line (@lines) {
		if ( $line=~/^(?!#)/ ) {
#			( $NLAY,$NROW,$NCOL,$NPER,$ITMUNI,$LENUNI )
#				= /'?(.+)'?(?:(?:\s*,\s*|\s+)'?(.+)'?){5}/;
			( $NLAY,$NROW,$NCOL,$NPER,$ITMUNI,$LENUNI )
				= quotewords( '\s*,\s*|\s+', 0, $line );
		last DIS1;
		}
		else {
			$j++;
		}
	}

	nono( $j,\@lines );

	$NCxR				= $NCOL*$NROW;
	$NCxL				= $NCOL*$NLAY;

	DIS2: {
		$j++;
#		@LAYCBD			= $lines[$j]
#						=~ /'?(.+)'?(?:(?:\s*,\s*|\s+)'?(.+)'?){\Q$NLAY-1}/;
# Trailing characters must be removed for proper use of quotewords
		@LAYCBD			= quotewords( '\s*,\s*|\s+', 0, $lines[$j] );
		@LAYCBD			= grep { defined && /\S/ } @LAYCBD;
#		DIS2lo: for my $m ( $#LAYCBD..0 ) {
#			@LAYCBD	= @LAYCBD[0..$m]	if ( defined $LAYCBD[$m]
#											&& $LAYCBD[$m]=~/\S/ );
#			last DIS2lo;
#		}
		die "Error: DIS2 LAYCBD - Non-zero bottom layer (cf 8-13)"
			unless ( $LAYCBD[0]==0 );
	}

	DIS3: {
		$j++;
		( my $DELR,$j )	= UDREL( \@lines,\@Nunits,\@Fnames,$NCOL,$j,\@leadcnt,
								\@trailcnt );
		my @DELR		= @$DELR;
	}

	DIS4: {
		$j++;
		( my $DELC,$j )	= UDREL( \@lines,\@Nunits,\@Fnames,$NROW,$j,\@leadcnt,
								\@trailcnt );
		my @DELC		= @$DELC;
	}
	
	DIS5: {
		$j++;
		( my $Top,$j )	= UDREL( \@lines,\@Nunits,\@Fnames,$NCxR,$j,\@leadcnt,
								\@trailcnt );
		my @Top			= @$Top;
	}

	DIS6: {
		$k				= 1;
		my @LAYCBDno	= grep { $_ != 0 } @LAYCBD	if ( $#LAYCBD>=1 );
		my $DIS6rep		= $NLAY + (scalar @LAYCBDno);
		my ( $BOTM,@BOTM );
		DIS6lo: while ( $k<=$DIS6rep ) {
			$j++;
			( $BOTMtmp,$j )	= UDREL( \@lines,\@Nunits,\@Fnames,$NCxR,$j,\@leadcnt,
								\@trailcnt );
			@BOTMtmp		= @$BOTMtmp;
			push ( @BOTM, @BOTMtmp );
			$k++;
		}
	}

	DIS7: {
		$k				= 1;
		my ( @DIS7,@PERLEN,@NSTP,@TSMULT,@SsTr );
		DIS7lo: while ( $k<=$NPER ) {
			$j++;
#			@DIS7		= $lines[$j]
#						=~ /'?(.+)'?(?:(?:\s*,\s*|\s+)'?(.+)'?){3}/;
			@DIS7		= quotewords( '\s*,\s*|\s+', 0, $lines[$j] );
			@DIS7		= grep { defined && /\S/ } @DIS7;
			push ( @PERLEN, $DIS7[0] );
			push ( @NSTP, $DIS7[1] );
			push ( @TSMULT, $DIS7[2] );
			die "Error: DIS7 Ss/Tr - Input not allowed (cf 8-13)"
				unless ( $DIS7[3]=~/Ss|Tr/i );
			push ( @SsTr, $DIS7[3] );
			$k++;
		}
	}

}

#####

#
######
## TCL
######
#
#DISwrite: {
#	my $tcl				= $prefix.".tcl";
#	$path				= $out.$tcl;
#	open($handle,">>",$path)
#		|| die "$0: can't open $path for writing: $!";
#	print $handle "#";
#	print $handle "# Import the ParFlow TCL package";
#	print $handle "#";
#	print $handle "lappend auto_path $env(PARFLOW_DIR)/bin";
#	print $handle "package require parflow";
#	print $handle "namespace import Parflow::*";
#	print $handle "#";
#	print $handle "# File input version number";
#	print $handle "#";
#	print $handle "pfset FileVersion 4";
#	print $handle "#";
#	print $handle "# Process Topology";
#	print $handle "#";
#	my $cpuno			= Sys::CPU::cpu_count();
#	my ( $ProcTopP,$ProcTopQ )	= floor( $cpuno/3 );
#	my $ProcTopR		= floor( $cpuno/3 ) + $cpuno%3;
#	print $handle "pfset Process.Topology.P\t$ProcTopP";
#	print $handle "pfset Process.Topology.Q\t$ProcTopQ";
#	print $handle "pfset Process.Topology.R\t$ProcTopR";
#	print $handle "#";
#	print $handle "# Computational Grid";
#	print $handle "pfset ComputationalGrid.Lower.X\t0.0";
#	print $handle "pfset ComputationalGrid.Lower.Y\t0.0";
#	print $handle "pfset ComputationalGrid.Lower.Z\t0.0";
#	print $handle "";
#	print $handle "pfset ComputationalGrid.NX\t$NCOL";
#	print $handle "pfset ComputationalGrid.NY\t$NROW";
#	print $handle "pfset ComputationalGrid.NZ\t$NLAY";
#	print $handle "";
#	if ( ( keys %{{ map {$_, 1} @DELR }} == 1 )
#		( keys %{{ map {$_, 1} @DELC }} == 1 ) ) {
#		print $handle "pfset ComputationalGrid.DX\t$DELR[0]";
#		print $handle "pfset ComputationalGrid.DX\t$DELC[0]";
#	}
#	else {
#		die "Error: TCL - Non-constant width computational grids unsupported";
#	}
## IMPLEMENT: for-loop true/false condition across BOTM
#	if ( ( keys %{{ map {$_, 1} @LAYCBD }} == 1 )
#		( keys %{{ map {$_, 1} @Top }} == 1 ) ) {
#		print $handle "pfset ComputationalGrid.DX\t";
#	}
#	else {
#		die "Error: TCL - Quasi-3D confining beds unsupported";
#	}
##		@lines	= <$handle>;
#	close($handle)
#		|| die "$path: $!";
#	$handle				= undef;
#}
#
######
#

#####
# BAS
#####

BASread: {
	$Ftype							= 'BAS6';
	( $lines,$leadcnt,$trailcnt )	= Fread( $Ftype,\@Ftypes,\@Fnames,$in );
	@lines							= @$lines;
	@leadcnt						= @$leadcnt;
	@trailcnt						= @$trailcnt;
}

BASasn: {
	
	$j								= 0;
	$k								= 1;

	BAS1: foreach my $line (@lines) {
		if ( $line=~/^(?!#)/ ) {
#			@Options	= $line
#						=~ /(XSECTION|CHTOCH|FREE)?(?:(?: )+(XSECTION|CHTOCH|FREE)){0,2}/i;
			@Options	= parse_line( ' ', 0, $line );
		last BAS1;
		}
		elsif ( $j < 2 ) {
			die "Error: BAS1 HEADNG - Length excessive (cf 8-14)"
				unless ( length($line) <= 80 );
			push ( my @HEADNG, $line );
			$j++;
		}
		else {
			die "Error: BAS1 Text - Length excessive (cf 8-14)"
				unless ( length($line) < 200 );
			$j++;
		}
	}

	nono( $j,\@lines );

	BAS2: {
		my ( $IBOUND,@IBOUND );
		$j++;
		if ( grep( /^XSECTION$/i, @Options ) ) {
			( $IBOUND,$j )	= U2DINT( \@lines,\@Nunits,\@Fnames,$NCxL,$j,\@leadcnt,
							\@trailcnt );
			@IBOUND			= @$IBOUND;
		}
		else {
			BAS2wh: while ( $k<=$NLAY ) {
				( $IBOUND,$j )	= U2DINT( \@lines,\@Nunits,\@Fnames,$NCxR,$j,\@leadcnt,
								\@trailcnt );
				@IBOUND	= @$IBOUND;
				$k++;
			}
		}
	}

	BAS3: {
		my $HNOFLO;
		$j++;
		if ( grep( /^FREE$/i, @Options ) ) {
			$HNOFLO	= $lines[$j]
					=~ /(.+)/;
		}
		else {
			$HNOFLO	= $lines[$j]
					=~ /(.{10})/;
		}
	}

	BAS4: {
		my ( $STRT,@STRT );
		$j++;
		if ( grep( /^XSECTION$/i, @Options ) ) {
			( $STRT,$j )	= UDREL( \@lines,\@Nunits,\@Fnames,$NCxL,$j,\@leadcnt,
							\@trailcnt );
			@STRT			= @$STRT;
		}
		else {
			BAS4wh: while ( $k<=$NLAY ) {
				( $STRT,$j )	= UDREL( \@lines,\@Nunits,\@Fnames,$NCxR,$j,\@leadcnt,
								\@trailcnt );
				@STRT			= @$STRT;
				$k++;
			}
		}
	}

#####


#####
# BCF
#####

BCFread: {
	$Ftype							= 'BCF6';
	( $lines,$leadcnt,$trailcnt )	= Fread( $Ftype,\@Ftypes,\@Fnames,$in );
	@lines							= @$lines;
	@leadcnt						= @$leadcnt;
	@trailcnt						= @$trailcnt;
}

BCFasn: {

	$j								= 0;
	$k								= 0;

	BCF1: foreach my $line (@lines) {
		if ( $line=~/^(?!#)/ && grep( /^FREE$/i, @Options ) ) {
			( $IBCFCB,$HDRY,$IWDFLG,$WETFCT,$IWETIT,$IHDWET )
					= quotewords( '\s*,\s*|\s+', 0, $line );
		last BAS1;
		}
		elsif ( $line=~/^(?!#)/ ) {
			( $IBCFCB,$HDRY,$IWDFLG,$WETFCT,$IWETIT,$IHDWET )
					= $line
					=~ m/.{10}/g;
		last BAS1;
		}
		else {
			j++;
		}
	}

	BCF2: {
		my @Ltype;
		if ( grep( /^FREE$/i, @Options ) ) {
			while ( $#Ltype < $NLAY ) {
				$j++;
				push( @Ltype, quotewords( '\s*,\s*|\s+', 0, $lines[$j] ) );
				@Ltype	= grep { defined && /\S/ } @Ltype;
			}
		}
		else {
			while ( $#Ltype < $NLAY ) {
				$j++;
				@Ltype	= $lines[$j] =~ m/.{2}/g;
			}
		}
	}

	BCF3: {
		$j++;
		( my $TRPY,$j )	= UDREL( \@lines,\@Nunits,\@Fnames,$NLAY,$j,\@leadcnt,
							\@trailcnt );
		my @TRPY		= @$TRPY;
	}

	my $TR	= grep( /^TR$/i, @SsTr );
	BCFwh: while ( $k < $NLAY ) {

		BCF4: {
			if ($TR) {
				$j++:
				( my $Sf1,$j )		= UDREL( \@lines,\@Nunits,\@Fnames,$NCxR,$j,\@leadcnt,
										\@trailcnt );
				my @Sf1				= @$Sf1;
			}
		}

		$j++;
		BCF5: {
			if ( $Ltype[$k]==0 || $Ltype[$k]==2 ) {
				( my $Tran,$j )		= UDREL( \@lines,\@Nunits,\@Fnames,$NCxR,$j,\@leadcnt,
										\@trailcnt );
				my @Tran			= @$Tran;
			}
		}
		BCF6: {
			else {
				( my $HY,$j )		= UDREL( \@lines,\@Nunits,\@Fnames,$NCxR,$j,\@leadcnt,
										\@trailcnt );
				my @HY				= @$HY;
			}
		}

		BCF7: {
			if ($k!=0) {
				$j++;
				( my $Vcont,$j )	= UDREL( \@lines,\@Nunits,\@Fnames,$NCxR,$j,\@leadcnt,
										\@trailcnt );
				my @Vcont			= @$Vcont;
			}
		}

		BCF8: {
			if ( $TR && ( $Ltype[$k]==2 || $Ltype[$k]==3 ) ) {
				$j++;
				( my $Sf2,$j )		= UDREL( \@lines,\@Nunits,\@Fnames,$NCxR,$j,\@leadcnt,
										\@trailcnt );
				my @Sf2				= @$Sf2;
			}
		}

		BCF9: {
			if ( IWDFLG!=0 && ( $Ltype[$k]==1 || $Ltype[$k]==3 ) ) {
				$j++;
				( my $WETDRY,$j )	= UDREL( \@lines,\@Nunits,\@Fnames,$NCxR,$j,\@leadcnt,
										\@trailcnt );
				my @WETDRY			= @$WETDRY;
			}
		}

		$k++;

	}


}


